﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace ElloitUI
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
                this.PopulateGridView();

        }

        protected void Search(object sender, EventArgs e)
        {
            this.PopulateGridView();
        }

        private void PopulateGridView()
        {
            string apiurl = "https://api.zoom.us/v2";
            string apikey = "dz6cOxStO8Zp55He7YZ9PT9zHD0tmLVYgQT9";
            WebClient client = new WebClient();
            //client.Headers["Content-type"] = "application/json";
            client.Headers.Add("Authentication-Token", apikey);
            client.Encoding = Encoding.UTF8;
            client.UseDefaultCredentials = true;
            client.Credentials = new NetworkCredential("invoice@kenzahn.com", "Education2018");
            string json = client.UploadString(apiurl + "/users", "");

            GridView1.DataSource = (new JavaScriptSerializer()).Deserialize<List<Users>>(json);
        }

        public class Users
        {
            public int id { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string email { get; set; }
        }
    }
}